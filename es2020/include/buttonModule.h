/*!
 * \file buttonModule.h
 * \brief This file's functions initialize the buttons and detect a button press.
 *
 * \author Lebedev
 *
 * \def B1_PRESSED
 * \brief A macro that checks if the 9th bit of the input data register of GPIOA is set to 0.
 *
 *
 * \def B2_PRESSED
 * \brief A macro that checks if the 7th bit of the input data register of GPIOC is set to 0.
 *
 * \def B1_B2_PRESSED
 * \brief A macro that checks if both, the 7th bit if GPIOC's IDR and the 9th bit of GPIOA's IDR are set to 0.
 */

#ifndef BUTTONMODULE_H_
#define BUTTONMODULE_H_

#include <stdbool.h>
#include <stm32l073xx.h>

#define B1_PRESSED		!(GPIOA->IDR & (1<<9))
#define B2_PRESSED		!(GPIOC->IDR & (1<<7))
#define B1_B2_PRESSED	(!(GPIOA->IDR & (1<<9))) && (!(GPIOC->IDR & (1<<7)))

/*!
 * \brief Configurates PA9 and PC7 as input for the button module.
 *
 * After execution the ports PA9 and PC7 can be used as inputs for the button module.
 * The function employs the GPIOA9 and GPIOA7 as input sources (MODER: 00).
 * For both ports the pull-up-pull-down-registers are set to the Pull-up mode (PUPDR: 01).
 */
void initButtons();


 /*!
 * \brief Is called to detect if a button was pressed
 *
 * The function detects what button(s) was (were) pressed.
 * The button must stay pressed while the minimal defined timespan in order for press to be validated.
 * A button press is recognized if it stayed pressed minButtonPressTime milliseconds or longer.
 *
 * \param minButtonPressTime - Integer, that represents the timespan in ms while a single button must stay presseds
 * \returns 0 if neither button 1 nor button 2 was pressed
 * \returns 1 if button 1 was pressed
 * \returns 2 if button 2 was pressed
 * \returns 12 is both buttons were pressed
 */
int buttonPressed(int minButtonPressTime);

#endif /* BUTTONMODULE_H_ */
