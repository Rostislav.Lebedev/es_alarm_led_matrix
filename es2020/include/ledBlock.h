/*!
 * \file ledBlock.h
 * \brief This file's functions initialize the LED and control them.
 */

#ifndef LEDBLOCK_H_
#define LEDBLOCK_H_

#include <stm32l073xx.h>

/*!
 * \brief Configurates the ports PC8, PB2 and PA6 as output for the LED.
 *
 * After being called this functions sets the ports PC8, PB2 and PA6 as output.
 * In the program, the ports PC8, PB2 and PA6 are used in order to control the green, blue and red LED.
 * All of the ports are set to output type "push-pull".
 * The PUPDR are set to 00 (no pull-up, pull-down) for all the ports.
 */
void initLEDblock();

/*!
 * \brief Switches the LED on and off.
 *
 * The function switches one of the LED on and switches all the other LED off according to the parsed parameter.
 * \param led - Character, that represents what LED must be switched on.
 * 				'b' Switches on the blue LED and switches off all the other.
 * 				'g' Switches on the green LED and switches off all the other.
 * 				'r' Switches on the red LED and switches off all the other.
 * 				'n' Switches off all the LED.
 */
void switchLED(char led);


#endif /* LEDBLOCK_H_ */
