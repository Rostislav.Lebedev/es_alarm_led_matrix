/*!
 * \file rgbmatrix.h
 * \brief This file's functions control the RGB LED matrix.
 * \author Juras
 */

#ifndef RGBMATRIX_H_
#define RGBMATRIX_H_

#ifdef __cplusplus
#include <cstdint>
extern "C" {
#else
#include <stdint.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define I2C_CMD_CONTINUE_DATA					0x81

#define RGBMATRIX_DEF_I2C_ADDR					0x65 /**< The device i2c address in default */
#define RGBMATRIX_VID 							0x2886 /**< Vender ID of the device */
#define RGBMATRIX_PID 							0x8005 /**< Product ID of the device */

#define I2C_CMD_GET_DEV_ID		    			0x00 /**< This command gets device ID information */
#define I2C_CMD_DISP_BAR		    			0x01 /**< This command displays LED bar */
#define I2C_CMD_DISP_EMOJI		    			0x02 /**< This command displays emoji */
#define I2C_CMD_DISP_NUM          				0x03 /**< This command displays number */
#define I2C_CMD_DISP_STR		    			0x04 /**< This command displays string */
#define I2C_CMD_DISP_CUSTOM		    			0x05 /**< This command displays user-defined pictures */
#define I2C_CMD_DISP_OFF		    			0x06 /**< This command cleans the display */
#define I2C_CMD_DISP_ASCII		    			0x07 /**< not use */
#define I2C_CMD_DISP_FLASH						0x08 /**< This command displays pictures which are stored in flash */
#define I2C_CMD_DISP_COLOR_BAR          		0x09 /**< This command displays colorful led bar */
#define I2C_CMD_DISP_COLOR_WAVE         		0x0a /**< This command displays built-in wave animation */
#define I2C_CMD_DISP_COLOR_CLOCKWISE    		0x0b /**< This command displays built-in clockwise animation */
#define I2C_CMD_DISP_COLOR_ANIMATION       		0x0c /**< This command displays other built-in animation */
#define I2C_CMD_DISP_COLOR_BLOCK                0x0d /**< This command displays an user-defined color */
#define I2C_CMD_STORE_FLASH						0xa0 /**< This command stores frames in flash */
#define I2C_CMD_DELETE_FLASH        			0xa1 /**< This command deletes all the frames in flash */

#define I2C_CMD_LED_ON			    			0xb0 /**< This command turns on the indicator LED flash mode */
#define I2C_CMD_LED_OFF			    			0xb1 /**< This command turns off the indicator LED flash mode */
#define I2C_CMD_AUTO_SLEEP_ON	    			0xb2 /**< This command enable device auto sleep mode */
#define I2C_CMD_AUTO_SLEEP_OFF	    			0xb3 /**< This command disable device auto sleep mode (default mode) */

#define I2C_CMD_DISP_ROTATE         			0xb4 /**< This command setting the display orientation */
#define I2C_CMD_DISP_OFFSET         			0xb5 /**< This command setting the display offset */

#define I2C_CMD_SET_ADDR		    			0xc0 /**< This command sets device i2c address */
#define I2C_CMD_RST_ADDR		    			0xc1 /**< This command resets device i2c address */
#define I2C_CMD_TEST_TX_RX_ON       			0xe0 /**< This command enable TX RX pin test mode */
#define I2C_CMD_TEST_TX_RX_OFF      			0xe1 /**< This command disable TX RX pin test mode */
#define I2C_CMD_TEST_GET_VER        			0xe2 /**< This command use to get software version */
#define I2C_CMD_GET_DEVICE_UID      			0xf1 /**< This command use to get chip id */


#ifdef RGBMATRIX_H_
enum orientation_type_t {
    DISPLAY_ROTATE_0 = 0,
    DISPLAY_ROTATE_90 = 1,
    DISPLAY_ROTATE_180 = 2,
    DISPLAY_ROTATE_270 = 3,
};
#endif


/*!
 * \brief The definition of the color on the RGB-Matrix
 *
 *  00 red |
 *  12 orange |
 *  18 yellow |
 *  52 green |
 *  7f cyan |
 *  aa blue |
 *  c3 purple |
 *  dc pink |
 *  fe white |
 *  ff black |
 */
enum COLORS {
    red = 0x00,
    orange = 0x12,
    yellow = 0x18,
    green = 0x52,
    cyan = 0x7f,
    blue = 0xaa,
    purple = 0xc3,
    pink = 0xdc,
    white = 0xfe,
    black = 0xff,
};

/*!
 * \brief Set the angle of Orientation for the LED-Matrix
 *
 * \param enum orientation_type_t the angles can be put into this function to tilt the screen
 * 	DISPLAY_ROTATE_0
 *  DISPLAY_ROTATE_90
 *  DISPLAY_ROTATE_180
 *  DISPLAY_ROTATE_270
 *
 * \returns none
 */
void setDisplayOrientation(enum orientation_type_t orientation);

/*!
 * \brief Display a bar on RGB LED Matrix.
 *
 *	\param uint8_t bar 0 - 32. 0 is blank and 32 is full.
 *  \param uint16_t duration_time Set the display time(ms) duration. Set it to 0 to not display
 *  \param bool forever_flag Set it to true to display forever, and the duration_time will not work.
 *  Or set it to false to display one time.
 *  \param uint8_t color Set the color of the display, range from 0 to 255. See COLORS for more details.
 *
 *  \return none
 */
void displayBar(uint8_t bar, uint16_t duration_time, bool forever_flag, uint8_t color);

/*!
 * \brief Display other built-in animations on RGB LED Matrix.
 * \param uint8_t index the index of animations:
 *			0. big clockwise
 *			1. small clockwise
 *			2. rainbow cycle
 *			3. fire
 *			4. walking child
 *			5. broken heart
 * \param uint16_t duration_time Set the display time(ms) duration. Set it to 0 to not display.
 * \param bool forever_flag Set it to true to display forever, and the duration_time will not work.
 *			      Or set it to false to display one time.
 * \return none
 */
void displayColorAnimation(uint8_t index, uint16_t duration_time, bool forever_flag);

void displayEmoji(uint8_t emoji, uint16_t duration_time, bool forever_flag);

/*!
 * \brief Display a number(-32768 ~ 32767) on LED matrix.
 *
 * \param int16_t number Set the number you want to display on LED matrix. Number(except 0-9)
 *            will scroll horizontally, the shorter you set the duration time,
 *			the faster it scrolls. The number range from -32768 to +32767, if
 *			you want to display larger number, please use displayString().
 * \param uint16_t duration_time Set the display time(ms) duration. Set it to 0 to not display.
 * \param bool forever_flagSet it to true to display forever, or set it to false to display one time.
 * \param uint8_t color Set the color of the display, range from 0 to 255. See COLORS for more details.
 * \return none
 */
void displayNumber(int16_t number, uint16_t duration_time, bool forever_flag, uint8_t color);

/*!
 * \brief Display a string on LED matrix.
 *
 * \param char* str The string pointer, the maximum length is 28 bytes. String will
 *		 scroll horizontally when its length is more than 1. The shorter
 *		 you set the duration time, the faster it scrolls.
 * \param uin16_t duration_time Set the display time(ms) duration. Set it to 0 to not display.
 * \param bool forever_flag Set it to true to display forever, or set it to false to display one time.
 * \param uint8_t color Set the color of the display, range from 0 to 255. See COLORS for more details.
 *
 * \return none
 */
void displayString(char* str, uint16_t duration_time, bool forever_flag, uint8_t color);

/*!
 * \brief Display user-defined frames on LED matrix.
 *
 * \param uint8_t* buffer The data pointer. 1 frame needs 64bytes data. Frames will switch
 *            automatically when the frames_number is larger than 1. The shorter
 *			you set the duration_time, the faster it switches.
 * \param uint16_t duration_time Set the display time(ms) duration. Set it to 0 to not display.
 * \param bool forever_flag Set it to true to display forever, or set it to false to display one time.
 * \param uint8_t frames_number the number of frames in your buffer. Range from 1 to 5.
 */
void displayFrames(uint8_t* buffer, uint16_t duration_time, bool forever_flag, uint8_t frames_number);

/*!
 * \brief Displays an array of frames.
 */
void displayFrames2(uint64_t* buffer, uint16_t duration_time, bool forever_flag, uint8_t frames_number);

/*!
 * \brief Display color block on LED matrix with a given uint32_t rgb color.
 *
 * \param uint32_t rgb the RGB values as HEX numbers 0xRRGGBB
 * \param uint16_t duration time Set the display time(ms) duration. Set it to 0 to not display.
 * \param bool forever_flag Set it to true to display forever, or set it to false to display one time.
 *
 * \return none
 */
void displayColorBlock(uint32_t rgb, uint16_t duration_time, bool forever_flag);

/*!
 * \brief Display nothing on LED Matrix.
 *
 * \return none
 */
void stopDisplay(void);

/*!
 * \brief Save the displayed LEDS on the LED Matrix
 *
 * \return none
 */
void storeFrames(void);

/*!
 * \brief Delete the frames which are stored on the flash
 *
 * \return none
 */
void deleteFrames(void);

/*!
 * \brief Show a Clock on the LED Matrix
 *
 * \return none
 */
void showClock();

/*!
 * \brief Show an exclamation mark on the LED Matrix in red
 *
 * \return none
 */
void showAlarm();

/*!
 * \brief Shows a clockwise Animation which durates 1 second
 *
 * \return none
 */
void clockTick();

/*!
 * \brief Show an arrow on the LED-Matrix in 2 different colors
 *
 * \param bool size The color of the arrow which is showed
 * 					true  Show a pink arrow
 * 					false Show an orange arrow
 *
 * \return none
 */
void showArrow(bool size);

/*!
 * \brief The function was used to test the matrix.
 * \deprecated Not in use anymore.
 */
void testMatrix(void);

/*!
 * \brief Displays the remaining time as a percentage on the RGB-LED matrix.
 *
 * The function calculates the remaining time after the timer was started.
 * Then, the function visualizes the remaining time using the matrix as lighted LED.
 * E.g., at the beginning, all 64 LED are lit up.
 * After 50% of the initial time has passed, only 32 LED display colour.
 *
 * \param allSeconds - Integer, that represents the initial time in seconds.
 * 		  allSeconds = minutes * 60 + seconds
 * \param remainingSeconds - Integer, that represents the remaining time in seconds.
 */
void rgb_led_show_remaining_time(int allSeconds, int remainingSeconds);

#endif
