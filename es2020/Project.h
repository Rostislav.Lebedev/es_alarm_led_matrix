/*!
\mainpage
\tableofcontents
\section sec1 Introduction
This project was completed within the scope of the course "Embedded Systems" at the Ostfalia, University of applied sciences.
The aim of the project was the implementation of an alarm clock using the STM32 microcontroller and a RGB-LED-matrix.

\section sec2 Project description
In the project, folowing hardware was used:
<ul>
    <li>sensor modules:
        <ul>
            <li>button module</li>
        </ul>
    </li>
    <li>actuator modules:
        <ul>
            <li>LCD</li>
            <li>RGB-LED-matrix</li>
        </ul>
    </li>
</ul>

\subsection sec21 Requirements
Before begin, folowing requirements were defined:
<ul>
    <li>In the idle state, a clock icon must be shown with the help of the RGB-LED-matrix.</li>
    <li>
        The user must be able to to setup the alarm time with the help of the button module that consists of two buttons.
        <ul>
            <li>Button 1 press must increment the seconds.</li>
            <li>Button 2 press must increment the minutes.</li>
            <li>The user must be able to confirm the alarm time with the press of both buttons simultaneously.</li>
        </ul>
    </li>
    <li>
        While the time runs to zero...
        <ul>
            <li>The LCD must show the remaining time.</li>
            <li>The RGB-LED-matrix shows the remaining time.</li>
            <li>The user must be able to interrupt this routine with a button press.</li>
        </ul>
    </li>
    <li>
        When the time is up...
        <ul>
            <li>The RGB-LED-matrix must show an exclamation mark.</li>
            <li>The LCD must demonstrate that the time is up.</li>
            <li>The user must be able to deactivate the alarm with a button press.</li>
        </ul>
    </li>
</ul>
\image html "img/hardware.jpg" "The end result" width=40%

\subsection sec21 State diagram
The system is always in one of the four states: state_idle, state_setTime, state_runTime or state_Alarm. The folowing state diagram visualizes the transitions between the states.
\image html "img/Zustandsdiagramm_allgemein.png" "The state diagram" width=40%

\subsection sec22 Hardware
\subsubsection sec221 Connection
The button module must de connected to the pins D8 (PA9) and D9 (PC7). The LCD and the RGB-LED-matrix both use the I2C connection.
\image html "img/connection.jpg" "Connection to the ports of the base shield" width=40%

\subsection sec23 The program
\subsubsection sec231 main.c
The folowing Nassi–Shneiderman diagram visualizes how the main.c works.
\image html "img/main_struc.png" "Nassi–Shneiderman diagram of main.c"

\subsubsection sec232 buttonModule.h & buttonModule.c
Both buttons must be initialized as inputs. In order to do so, it's necessary to configurate the GPIOA_MODER and GPIOC_MODER registers.
For both buttons the GPIOx_PUPDR registers are set up into the Pull-up-mode. The initialization is done in initButtons().
In order to recognize what button was pressed, the function buttonPressed(int minButtonPressTime) was written.
The function decides what button was pressed and returns a button identifier as an integer.
The folowing Nassi–Shneiderman diagram visualizes the necessary steps of button recognition.
\image html "img/int_buttonPressed.png" "Nassi–Shneiderman diagram of buttonPressed(int minButtonPressTime)"


*/