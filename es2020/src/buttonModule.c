/*!
 * \file buttonModule.c
 * \brief The implementation of buttonModule.h header file.
 */
#include "buttonModule.h"
#include "lcd.h"

void initButtons(){
	// b1
	// D8 = PA9
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
	GPIOA->MODER &= ~((1<<18) | (1<<19));
	GPIOA->PUPDR |= (1<<18);	// pull-up

	// b2
	// D9 = PC7
	RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
	GPIOC->MODER &= ~((1<<14) | (1<<15));
	GPIOC->PUPDR |= (1<<14);	// pull-up
}

int buttonPressed(int minButtonPressTime){
	int ret = 0;
	int sampleTime 		= 10;			// ms
	int realPressTimeB1	= 0;			// ms
	int realPressTimeB2	= 0;			// ms

	if(B1_PRESSED){
		do{
			if(B2_PRESSED){
				ret = 12;					// b2 was pressed after b1 was pressed
				break;
			}
			delay_ms(sampleTime);
			realPressTimeB1 += sampleTime;

			if(realPressTimeB1 >= minButtonPressTime)
				break;						// leave the loop if b1 was pressed long enough

		}while(B1_PRESSED);
	}
	else if(B2_PRESSED){
		do{
			if(B1_PRESSED){
				ret = 12;					// b1 was pressed after b2 was pressed
				break;
			}
			delay_ms(sampleTime);
			realPressTimeB2 += sampleTime;

			if(realPressTimeB2 >= minButtonPressTime)
				break;						// leave the loop if b2 was pressed long enough

		}while(B2_PRESSED);
	}

	if(ret != 12){						// what button was pressed, if not both?
		if(realPressTimeB1 >= minButtonPressTime){
			ret = 1;						// b1 press validated
		}
		else if(realPressTimeB2 >= minButtonPressTime){
			ret = 2;						// b2 press validated
		}
	}

	return ret;
}



