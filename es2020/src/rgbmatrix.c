/*!
 * \file rgbmatrix.c
 * \brief The implementation of rgbmaxtrix.h header file.
 *
 */

#include "rgbmatrix.h"
#include "delay.h"
#include "clock.h"
#include "i2c.h"
#include "systick.h"
#include <stdbool.h>
#include <stddef.h>


/*!
 * \brief Send one byte of data to the given I2C address
 *
 * \param uint8_t address The I2C address where the data is send to
 * \param uint8_t data The data which has to be send
 * \returns none
 */
void i2cSendByte(uint8_t address, uint8_t data)
{
	i2c_transfer(address, &data, 1, NULL, 0);
}

/*!
 * \brief Send a amount of bytes to the I2C device
 *
 * \param uint8_t address The I2C address where the data is send to
 * \param uint8_t *data The data which has to be send, it can be an array of uint8_t data
 * \param uint8_t len The length of the data which are send in bytes
 *
 * \returns none
 */
void i2cSendBytes(uint8_t address, uint8_t* data, uint8_t len)
{
	i2c_transfer(address, data, len, NULL, 0);
}

/*!
 * \brief Send a amount of bytes to the I2C device.
 * Some functions use it for better programming syntax
 *
 * \param uint8_t address The I2C address where the data is send to
 * \param uint8_t *data The data which has to be send, it can be an array of uint8_t data
 * \param uint8_t len The length of the data which are send in bytes
 *
 * \returns none
 */
void i2cSendContinueBytes(uint8_t address, uint8_t* data, uint8_t len) {
    uint8_t cbytes = I2C_CMD_CONTINUE_DATA;

    uint8_t *send[2] = {&cbytes,data};


    i2c_transfer(address, *send, len+1, NULL, 0);
}

void setDisplayOrientation(enum orientation_type_t orientation)
{
    uint8_t data[2] = {0, };

    data[0] = I2C_CMD_DISP_ROTATE;
    data[1] = (uint8_t)orientation;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 2);
}

void displayBar(uint8_t bar, uint16_t duration_time, bool forever_flag, uint8_t color) {
    uint8_t data[6] = {0, };

    data[0] = I2C_CMD_DISP_BAR;

    if (bar > 32) {
        bar = 32;
    }
    data[1] = bar;
    data[2] = (uint8_t)(duration_time & 0xff);
    data[3] = (uint8_t)((duration_time >> 8) & 0xff);
    data[4] = forever_flag;
    data[5] = color;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 6);
}

void displayColorAnimation(uint8_t index, uint16_t duration_time, bool forever_flag) {
    uint8_t data[6] = {0, };
    uint8_t from, to;
    data[0] = I2C_CMD_DISP_COLOR_ANIMATION;
    switch (index) {
        case 0:
            from = 0;
            to = 28;
            break;

        case 1:
            from = 29;
            to = 41;
            break;

        case 2:				// rainbow cycle
            from = 255;
            to = 255;
            break;

        case 3: 			// fire
            from = 254;
            to = 254;
            break;

        case 4: 			// walking
            from = 42;
            to = 43;
            break;

        case 5:				// broken heart
            from = 44;
            to = 52;
            break;

        default:
            break;
    }

    data[1] = from;
    data[2] = to;
    data[3] = (uint8_t)(duration_time & 0xff);
    data[4] = (uint8_t)((duration_time >> 8) & 0xff);
    data[5] = forever_flag;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 6);
}

/*!
 * \brief Display emoji on LED matrix.
 *
 * \param uint8_t emoji Set a number from 0 to 29 for different emoji.
 * 				0	smile	10	heart		    20	house
 *				1	laugh	11	small heart		21	tree
 *				2	sad	    12	broken heart	22	flower
 *				3	mad	    13	waterdrop		23	umbrella
 *				4	angry	14	flame		    24	rain
 *				5	cry	    15	creeper		    25	monster
 *				6	greedy	16	mad creeper		26	crab
 *				7	cool	17	sword		    27	duck
 *				8	shy	    18	wooden sword	28	rabbit
 *				9	awkward	19	crystal sword	29	cat
 *				30  up		31  down			32 left
 * \param uint16_t duration_time Set the display time(ms) duration. Set it to 0 to not display
 * \param bool forever_flag Set it to true to display forever, and the duration_time will not work.
 *			      Or set it to false to display one time.
 * \return none
 */
void displayEmoji(uint8_t emoji, uint16_t duration_time, bool forever_flag) {
    uint8_t data[5] = {0, };

    data[0] = I2C_CMD_DISP_EMOJI;
    data[1] = emoji;
    data[2] = (uint8_t)(duration_time & 0xff);
    data[3] = (uint8_t)((duration_time >> 8) & 0xff);
    data[4] = forever_flag;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 5);
}

void displayNumber(int16_t number, uint16_t duration_time, bool forever_flag,
        uint8_t color) {
    uint8_t data[7] = {0, };
    data[0] = I2C_CMD_DISP_NUM;
    data[1] = (uint8_t)((uint16_t)number & 0xff);
    data[2] = (uint8_t)(((uint16_t)number >> 8) & 0xff);
    data[3] = (uint8_t)(duration_time & 0xff);
    data[4] = (uint8_t)((duration_time >> 8) & 0xff);
    data[5] = forever_flag;
    data[6] = color;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 7);
}

void displayString(char* str, uint16_t duration_time, bool forever_flag, uint8_t color) {
    uint8_t data[36] = {0, };
    uint8_t len = sizeof(str)/sizeof(str[0]);

    if (len >= 28) {
        len = 28;
    }

    for (uint8_t i = 0; i < len; i++) {
        data[i + 6] = str[i];
    }

    data[0] = I2C_CMD_DISP_STR;
    data[1] = forever_flag;
    data[2] = (uint8_t)(duration_time & 0xff);
    data[3] = (uint8_t)((duration_time >> 8) & 0xff);
    data[4] = len;
    data[5] = color;

    if (len > 25) {
        i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 31);
        delay_ms(1);
        i2cSendContinueBytes(RGBMATRIX_DEF_I2C_ADDR, data + 31, (len - 25));
    } else {
        i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, (len + 6));
    }

}

void displayFrames(uint8_t* buffer, uint16_t duration_time, bool forever_flag,
        uint8_t frames_number) {
    uint8_t data[72] = {0, };
    // max 5 frames in storage
    if (frames_number > 5) {
        frames_number = 5;
    } else if (frames_number == 0) {
        return;
    }

    data[0] = I2C_CMD_DISP_CUSTOM;
    data[1] = 0x0;
    data[2] = 0x0;
    data[3] = 0x0;
    data[4] = frames_number;

    for (int i = frames_number - 1; i >= 0; i--) {
        data[5] = i;
        for (int j = 0; j < 64; j++) {
            data[8 + j] = buffer[j + i * 64];
        }
        if (i == 0) {
            // display when everything is finished.
            data[1] = (uint8_t)(duration_time & 0xff);
            data[2] = (uint8_t)((duration_time >> 8) & 0xff);
            data[3] = forever_flag;
        }
        i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 24*3);
        delay_ms(1);
        }
}

void displayFrames2(uint64_t* buffer, uint16_t duration_time, bool forever_flag,
        uint8_t frames_number) {
    uint8_t data[72] = {0, };
    // max 5 frames in storage
    if (frames_number > 5) {
        frames_number = 5;
    } else if (frames_number == 0) {
        return;
    }

    data[0] = I2C_CMD_DISP_CUSTOM;
    data[1] = 0x0;
    data[2] = 0x0;
    data[3] = 0x0;
    data[4] = frames_number;

    for (int i = frames_number - 1; i >= 0; i--) {
        data[5] = i;
        // different from uint8_t buffer
        for (int j = 0; j < 8; j++) {
            for (int k = 7; k >= 0; k--) {
                data[8 + j * 8 + (7 - k)] = ((uint8_t*)buffer)[j * 8 + k + i * 64];
            }
        }

        if (i == 0) {
            // display when everything is finished.
            data[1] = (uint8_t)(duration_time & 0xff);
            data[2] = (uint8_t)((duration_time >> 8) & 0xff);
            data[3] = forever_flag;
        }
        i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 24*3);
        delay_ms(1);
        /*
        i2cSendContinueBytes(RGBMATRIX_DEF_I2C_ADDR, data + 24, 24);
        delay_ms(1);
        i2cSendContinueBytes(RGBMATRIX_DEF_I2C_ADDR, data + 48, 24);*/
    }
}

void displayColorBlock(uint32_t rgb, uint16_t duration_time, bool forever_flag) {
    uint8_t data[7] = {0, };
    data[0] = I2C_CMD_DISP_COLOR_BLOCK;
    // red green blue
    data[1] = (uint8_t)((rgb >> 16) & 0xff);
    data[2] = (uint8_t)((rgb >> 8) & 0xff);
    data[3] = (uint8_t)(rgb & 0xff);

    data[4] = (uint8_t)(duration_time & 0xff);
    data[5] = (uint8_t)((duration_time >> 8) & 0xff);
    data[6] = forever_flag;

    i2cSendBytes(RGBMATRIX_DEF_I2C_ADDR, data, 7);
}

void stopDisplay(void) {
    i2cSendByte(RGBMATRIX_DEF_I2C_ADDR, I2C_CMD_DISP_OFF);
}

void storeFrames(void) {
    i2cSendByte(RGBMATRIX_DEF_I2C_ADDR, I2C_CMD_STORE_FLASH);
    delay_ms(200);
}

void deleteFrames(void) {
    i2cSendByte(RGBMATRIX_DEF_I2C_ADDR, I2C_CMD_DELETE_FLASH);
    delay_ms(200);
}

void showClock()
{

	uint64_t clock[] = {
	  0xffff00000000ffff,
	  0xff00ffffffff00ff,
	  0x00ffffffff7fff00,
	  0x00ffffff7fffff00,
	  0x00ffff52ffffff00,
	  0x00ffff52ffffff00,
	  0xff00ff52ffff00ff,
	  0xffff00000000ffff
	};


	displayFrames(clock, 0, true, 1);

}

void showAlarm()
{
	uint64_t alarm[] = {
	  0xffffff0000ffffff,
	  0xffffff0000ffffff,
	  0xffffff0000ffffff,
	  0xffffff0000ffffff,
	  0xffffff0000ffffff,
	  0xffffffffffffffff,
	  0xffffff0000ffffff,
	  0xffffff0000ffffff
	};


	displayFrames(alarm, 0, true, 1);
}

void clockTick()
{
	displayColorAnimation(0,1000,true);
}

void showArrow(bool size)
{
	if(size == true){

		uint64_t arrow[] = {
			  0xffffffdcdcffffff,
			  0xffffdcdcdcdcffff,
			  0xffdcdcdcdcdcdcff,
			  0xdcdcdcdcdcdcdcdc,
			  0xffffffdcdcffffff,
			  0xffffffdcdcffffff,
			  0xffffffdcdcffffff,
			  0xffffffdcdcffffff
			};
		displayFrames(arrow, 0, true, 1);
	}
	else
	{
		uint64_t arrow[] = {
			  0xffffff1212ffffff,
			  0xffff12121212ffff,
			  0xff121212121212ff,
			  0x1212121212121212,
			  0xffffff1212ffffff,
			  0xffffff1212ffffff,
			  0xffffff1212ffffff,
			  0xffffff1212ffffff
			};
		displayFrames(arrow, 0, true, 1);
	}
}

void testMatrix()
{
	//funktioniert
	//displayColorBlock(0xff0000, 2000, false);

	showClock();

	//uint8_t clock[]= example[0];

	//displayEmoji(4,2000,false);
	//displayFrames(example, 0,false, 1);
	//storeFrames();

/*

	displayNumber(10,1000,false,blue);
	delay_ms(2000);

	for(int i = 0; i<33;i++)
	{
		displayBar(i,500, false, orange);
		delay_ms(500);
	}
*/
}

void rgb_led_show_remaining_time(int allSeconds, int remainingSeconds)
{
	int cells = 64;
	double secondsPercentage = (double)remainingSeconds/(double)allSeconds*100;
	int activeCells = (secondsPercentage/100)*cells;
	int lines = activeCells/8;
	int addCells = activeCells-lines*8;
	int i = 0;

	uint64_t blackLine = 0xffffffffffffffff;
	uint64_t cyanLine = 0x7f7f7f7f7f7f7f7f;
	uint8_t incompleteLine_8[8] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
	uint64_t incompleteLine_64 = blackLine;
	uint64_t frame[8];

	for(int i = 0; i<addCells; i++){
		incompleteLine_64 = incompleteLine_64 << 8;
		incompleteLine_64 += 0x7f;
	}

	for (; i<lines; i++) {
		frame[i] = cyanLine;
	}

	frame[i] = incompleteLine_64;
	i++;

	for (; i<=8; i++){
		frame[i] = blackLine;
	}

	displayFrames(frame,0,true,1);
}

