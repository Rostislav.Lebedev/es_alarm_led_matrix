/*!
 * \file main.c
 * \brief The starting point of the program.
 *
 * \author Juras
 * \author Lebedev
 */
#include "clock.h"
#include "systick.h"
#include "i2c.h"
#include "lcd.h"
#include <stm32l073xx.h>
#include "rgbmatrix.h"
#include "buttonModule.h"
#include "ledBlock.h"

//#include "keypad.h"

/*!
 * \brief The execution of the program starts here.
 *
 * In the function, all the necessary initialization are done and alarm functionality is implemented.
 */
int main(void) {
	clock_setup_16MHz();
	systick_setup();
	i2c_setup();
	lcd_init();
	lcd_clear_display();
	lcd_print_string("    Embedded    "
	                 "    Systems!!   ");

	initButtons();						// initialization of D8 and D9 as input
	initLEDblock();

	bool state_idle = true;
	bool state_runTime = false;
	bool state_setTime = false;
	bool state_Alarm = false;
	int minutes = 0;
	int seconds = 0;
	int allSeconds = 0;
	int minButtonPressTime = 100;		// timespan in ms while a button must stay pressed
	while (1)
	{
		/* STATE IDLE */
		while(state_idle) {

			switchLED('n');

			showClock();

			lcd_set_cursor(0,0);
			lcd_print_string("to set up time");
			lcd_set_cursor(1,0);
			lcd_print_string("press button 2");
			delay_ms(100);

			if(buttonPressed(minButtonPressTime) == 2) {
				lcd_clear_display();
				minutes = 0;
				seconds = 0;
				state_idle = false;
				state_setTime = true;

				/* STATE SETTIME */
				while(state_setTime){

					switchLED('g');

					if(buttonPressed(minButtonPressTime) == 1) {
						minutes++;
						showArrow(true);
					}
					else if(buttonPressed(minButtonPressTime) == 2) {
						seconds++;
						showArrow(false);
						if(seconds >= 60){
							minutes++;
							seconds = 0;
						}
					}
					else if(buttonPressed(minButtonPressTime) == 12) {
						allSeconds = minutes*60+seconds;
						if(allSeconds != 0) {
							state_setTime = false;
							state_runTime = true;
						}
						else {
							state_setTime = false;
							state_runTime = false;
							state_idle = true;
						}
						break;
					}
					lcd_update_state_setTime(minutes,seconds);
					delay_ms(50);
				}
			}
		}

		/* STATE RUNTIME */
		while(state_runTime)
		{
			switchLED('b');

			if((buttonPressed(minButtonPressTime) == 1) ||
			   (buttonPressed(minButtonPressTime) == 2))
			{
				state_runTime = false;
				state_idle = true;
				state_Alarm = false;
				break;
			}
			else {
				if(minutes >= 0 && seconds >= 0) {
					lcd_update_state_runTime(minutes,seconds);
					//displayColorAnimation(0,1000,true);
					rgb_led_show_remaining_time(allSeconds, minutes*60+seconds);
					if(seconds == 0) {

						if(minutes != 0){
							minutes--;
							seconds = 59;
						}
					}
					seconds--;
					delay_ms(1000);
				}
				else {
					state_runTime = false;
					state_Alarm = true;
				}
			}
		}

		/* STATE ALARM */
		while(state_Alarm)
		{
			int i = 10;
			while(i > 0) {
				lcd_clear_display();
				stopDisplay();
				switchLED('n');
				delay_ms(300);
				lcd_print_string("     Alarm!     "
							     "     Alarm!     ");
				showAlarm();
				switchLED('r');
				delay_ms(1000);
				i--;

				if((buttonPressed(minButtonPressTime) == 1) ||
				   (buttonPressed(minButtonPressTime) == 2))
				{
					break;
				}
			}

			state_Alarm = false;
			state_idle = true;
		}
	}
}
