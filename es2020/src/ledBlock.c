/*!
 * \file ledBlock.c
 * \brief This file's functions implement the ledBlock.h header file
 */

#include "ledBlock.h"

void initLEDblock() {
	// green led on PC8
	RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
	GPIOC->MODER = (GPIOC->MODER & ~GPIO_MODER_MODE8)
			| ((1u << GPIO_MODER_MODE8_Pos) & GPIO_MODER_MODE8_Msk);
	GPIOC->OTYPER &= ~GPIO_OTYPER_OT_8;
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPD8;

	// blue led on PB2
	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
	GPIOB->MODER = (GPIOB->MODER & ~GPIO_MODER_MODE2)
			| ((1u << GPIO_MODER_MODE2_Pos) & GPIO_MODER_MODE2_Msk);
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT_2;
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD2;

	// red led on PA6
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
	GPIOA->MODER = (GPIOA->MODER & ~GPIO_MODER_MODE6)
			| ((1u << GPIO_MODER_MODE6_Pos) & GPIO_MODER_MODE6_Msk);
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_6;
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPD6;
}

void switchLED(char led) {
	if(led == 'g') {
		//turn the green led on
		GPIOC->ODR |= (1<<8);

		//turn all the other led off
		GPIOA->ODR &= (0<<6);
		GPIOB->ODR &= (0<<2);
	}
	else if (led == 'b') {
		//turn the blue led on
		GPIOB->ODR |= (1<<2);

		//turn all the other led off
		GPIOA->ODR &= (0<<6);
		GPIOC->ODR &= (0<<8);
	}
	else if(led == 'r') {
		// turn red led on
		GPIOA->ODR |= (1<<6);

		// turn all the other led off
		GPIOC->ODR &= (0<<8);
		GPIOB->ODR &= (0<<2);
	}
	else if (led == 'n') {
		//turn off all the led
		GPIOA->ODR &= (0<<6);
		GPIOC->ODR &= (0<<8);
		GPIOB->ODR &= (0<<2);
	}
}

